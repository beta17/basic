var app = angular.module("sampleapp", ["ngRoute"]);
var data;
app.config(function($routeProvider) {
    $routeProvider
    
    .when("/red", {
        templateUrl : "red.html"
    })
    .when("/green", {
        templateUrl : "green.html"
    })
    .when("/blue", {
        templateUrl : "blue.html"
    });
});
app.controller("maincontrol", function ($scope, $http) {
    $http.get("http://localhost:8010/feed.json").then(function (response) {
        $scope.flag = "data";
        data = response.data;
        $scope.maxicon = data.length;
        $scope.iconval = 0;
        $scope.obj = [];
        for (let i = 0; i < data.length; i++) {
            let myobj = {};
            myobj.object = JSON.parse(data[i].object);
            $scope.obj.push(myobj);
        }
        $scope.msg = [];
        for (let i = 0; i < 6; i++) {
            let myobj = {};
            myobj.object = JSON.parse(data[i].object);
            myobj.id = JSON.parse(data[i].id);
            $scope.msg.push(myobj);
        }
        
    });

    $scope.ale = function () {
        $scope.msg = [];
        for (
            let i = (event.target.value - 1) * 6;
            i < event.target.value * 6;
            i++
        ) {
            let myobj = {};
            myobj.object = JSON.parse(data[i].object);
            myobj.id = JSON.parse(data[i].id);
            $scope.msg.push(myobj);
        }
       
    };
    $scope.next = function () {
        $scope.iconval++;
        let z = document.getElementById("previous").className;
        let x = z.replace(" disabled", "");
        document.getElementById("previous").className = x;
        if ($scope.iconval + 6 > $scope.maxicon / 6) {
            event.target.parentElement.className =
                event.target.parentElement.className + " " + "disabled";
            let z = document.getElementById("previous").className;
            let x = z.replace(" disabled", "");
            document.getElementById("previous").className = x;
        }
    };
    $scope.previous = function () {
        $scope.iconval--;
        let z = document.getElementById("next").className;
        let x = z.replace(" disabled", "");
        document.getElementById("next").className = x;
        if ($scope.iconval < 1) {
            event.target.parentElement.className =
                event.target.parentElement.className + " " + "disabled";
        }
    };
    $scope.topFunction = function () {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    };
    $scope.a = function () {
        if (event.target.style.color == "red") {
            event.target.style.color = "black";
        } else {
            event.target.style.color = "red";
        }
    };
    $scope.openNav = function () {
        
        $scope.popdata = [];
        for (let i = 0; i < $scope.msg.length; i++) {
            if ($scope.msg[i].id == parseInt(event.target.id)) {
                let myobj = {};
                myobj.object = $scope.msg[i].object;
                $scope.popdata.push(myobj);
            }
        }
       
        document.getElementById("mySidenav").style.width = "500px";
    };
    $scope.closeNav = function () {
        document.getElementById("mySidenav").style.width = "0";
    };
    $scope.comments = function () {
        $scope.comment = prompt("enter comment");
    };
    $scope.search=function()
    {
        $scope.datas=[];
        var flag=0;
        let val=document.getElementById("searchid").value;
        for(let i=0;i<data.length;i++){
        if(JSON.parse(data[i].id)==val){
            let myobj = {};
            myobj.object = JSON.parse(data[i].object);
            myobj.id = JSON.parse(data[i].id);
        $scope.datas.push(myobj);
        flag=1;
        break;
        }
        
    }
    if(flag==0){
        alert("please enter correct id")
    }
 
    }
});
